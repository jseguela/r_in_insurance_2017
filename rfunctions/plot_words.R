
####################################################################
#############         jaccard_similarity FUNCTION    ###############
####################################################################

##### Description :
#     This function allows to compute Jaccard similarity between the terms of a term-document matrix

##### Arguments :
#     tdm : term-document matrix created with TermDocumentMatrix function in tm package

jaccard_similarity <- function(tdm) {
  
  library(slam)
  library(tm)
  library(Matrix)
  
  tdm <- weightBin(tdm) # transformation to a binary matrix
  
  m <- sparseMatrix(i = tdm$i, j = tdm$j, x = tdm$v,
                    dims = dim(tdm),
                    dimnames = list(rownames(tdm), colnames(tdm)))
					
  # Jaccard[i,j] = #common / (#i + #j - #common)
  A <- tcrossprod(m) # common values
  im <- which(A > 0, arr.ind=TRUE, useNames = F) # indexes for non-zero common values
  b <- rowSums(m) # counts for each row
  Aim <- A[im] # only non-zero values of common
  xtemp <- Aim / (b[im[,1]] + b[im[,2]] - Aim)
  
  # keep a single value for each combination of terms
  it <- im[,1][im[,1] > im[,2]]
  jt <- im[,2][im[,1] > im[,2]]
  xt <- xtemp[im[,1] > im[,2]]
  
  simple_triplet_matrix(
    i = it,
    j = jt,
    v = xt,
    nrow = nrow(A), ncol = ncol(A),
    dimnames = list(rownames(tdm), rownames(tdm))
  )
}



####################################################################
#############           plot.words FUNCTION          ###############
####################################################################

##### Description :
#     This function allows to plot the co-occurrence graph of terms from a TDM.  
#     Edges are drawn according to the Jaccard similarity between terms. 

##### Arguments :
#     tdm :  term-document matrix created with TermDocumentMatrix function in tm package
#     nodeMinFreq : minimum number of documents to plot a term
#     edgeMinSim : minimum similarity to plot an edge between two terms
#     df.group : optional dataframe to add a color to the nodes according to a group variable (2 columns : "term" and "group")

plot.words <- function(tdm, nodeMinFreq, edgeMinSim, df.group = NULL){
  
  library(slam)
  library(tm)
  library(visNetwork)
  term.doc.freq <- data.frame(term = rownames(tdm), 
                              frequency = slam::row_sums(weightBin(tdm)))
  
  ### nodes
  nodes <- term.doc.freq %>% 
    mutate(id = gsub(" ", "_", term),
           value = frequency,
           label = term,
           title = paste("<b>", term, ":</b><br>", value)) %>% 
    filter(value >= nodeMinFreq) %>% 
    select(id, label, value, title)
  
  if(!is.null(df.group) && sum(names(df.group) %in% c("term", "group")) == 2) {
    nodes <- nodes %>% left_join(df.group, by = c("id" = "term"))
  }
  
  wordsim <- jaccard_similarity(tdm)
  
  ### edges
  edges <- data.frame(from = rownames(tdm)[wordsim$i], 
                      to = rownames(tdm)[wordsim$j],
                      width = 12*wordsim$v,
                      title = paste("sim = <b>", round(wordsim$v, 2), "</b>"))
  
  edges <- edges %>% filter(width >= 12*edgeMinSim) 
  
  # graph
  visNetwork(nodes, edges) %>% 
    visEdges(color = list(color = "rgba(200,200,200,0.5)", highlight = "darkgrey", 
                          hover = "darkgrey")) %>% 
    visInteraction(hover = T, hoverConnectedEdges = T, selectConnectedEdges = T) %>% 
    visOptions(highlightNearest = list(enabled = T, degree = list(from = 1, to = 1), 
                                       hover = T)) %>% 
    visLegend(enabled = T, useGroups = T)
}
