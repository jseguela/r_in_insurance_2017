
### How to reproduce

I highly recommend the use of RStudio (> 1.0).

1. Clone the project **r_in_insurance_2017**.
2. Click on **r_in_insurance_2017.Rproj** to open the project in RStudio. All paths are relative to this root.
3. **r_in_insurance.Rmd** contains the codes of my presentation. It's a R notebook, you can either knit it or run the chunks inside RStudio.

**data/**: contains datasets in rds format

**rfunctions/**: contains tool functions (to compute specific terms and to plot the co-occurrence network)

**figures/**: figures used in the notebook


### Data source

Since I started the project, the number of registered accidents has increased a lot, so the dataset is not up-to-date with respect to the source website (<http://www.aria.developpement-durable.gouv.fr/?lang=en>)

Original texts are written in French language, but I Google translated some words and texts to english language 
(I apologize if there are some mistakes!)


### Datasets

For size issues, datasets have been converted to the rds format.

- *acc.rds*: expert reports (for 28,260 accidents)
- *df_causes.rds*: reports cut into sentences (for manufacturing industry only)
- *dic_lemm.rds*: dictionary used for lemmatization (built thanks to treetag function in koRpus package)
- *dic_trans.rds*: dictionary used for translation (from french to english)


### Method

All analysis are made on french documents, and possibly translated to english language so that anyone can understand the meaning.

You can refer to the notebook for the details of the approach.

**Caution:** 
- Clustering is never perfect, you may have to go deeper and iterate again if you want to increase the homogeneity of your clusters.
- CTM (topic modeling) is a time consuming algorithm (computing time increase with the number of topics)

